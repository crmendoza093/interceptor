﻿using InterceptorWS.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InterceptorWS.Helper
{
    public class Rules
    {
        const string COMMERCIAL_NAME = "Empresa Confidencial";
        const int COMMERCIAL_ID = 0;

        public dynamic SensitiveDataRule(string payload)
        {
            dynamic oInput = JObject.Parse(payload);

            //Show the companyid = 0 when the company is confidential
            foreach (var item in oInput.Data.SimilarOffers)
            {
                item.CompanyId = !(bool)item.PublishCompanyName ? 0 : item.CompanyId;
            }

            if (!(bool)oInput.Data.JobOfferData.PublishCompanyName)
            {
                oInput.Data.CompanyData.CommercialName = COMMERCIAL_NAME;
                oInput.Data.CompanyData.Profile = string.Empty;
                oInput.Data.CompanyData.Id = COMMERCIAL_ID;

                if (oInput.Data.JobOfferData.ListAssociatedUniversities.Count > 0)
                {
                    foreach (var item in oInput.Data.JobOfferData.ListAssociatedUniversities)
                    {
                        item.Name = string.Empty;
                    }
                }
            }

            return oInput;
        }

        public dynamic ConfidencialOffer(string payload, List<OfferId> OfferList)
        {
            dynamic oInput = JObject.Parse(payload);

            foreach (var item in oInput.Data)
            {
                foreach (var itemOffer in OfferList)
                {
                    if (item.JobOfferId == itemOffer.Id)
                    {
                        item.CompanyId = COMMERCIAL_ID;
                        item.CompanyName = COMMERCIAL_NAME;
                    }
                }
            }

            return oInput;
        }
    }
}
