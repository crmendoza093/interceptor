﻿using System;
using System.Configuration;
using System.Net.Http;
namespace InterceptorWS.HTTP_Client
{
    public class WebApiClient
    {
        public string GetOfferData(int OfferId, bool isLoggued, int PersonId)
        {
            string ResourceName = "api/jobofferdetail/getjobofferdata";
            string ResourceURL = $"{ResourceName}?jobOfferId={OfferId}&isLoggued={isLoggued}&personId={PersonId}";

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(URL());
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                //HTTP GET
                var responseTask = client.GetAsync(ResourceURL);
                responseTask.Wait();
                var result = responseTask.Result;
                
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsStringAsync();
                    return readTask.Result;
                }
                return null;
            }
        }

        public string GetLastOffer(int CompanyId, int TotalOffers)
        {
            string ResourceName = "api/CompanyBase/getlastoffersapi";
            string ResourceURL = $"{ResourceName}?companyId={CompanyId}&totalOffers={TotalOffers}";

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(URL());
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                //HTTP GET
                var responseTask = client.GetAsync(ResourceURL);
                responseTask.Wait();
                var result = responseTask.Result;

                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsStringAsync();
                    return readTask.Result;
                }
                return null;
            }
        }

        protected string URL()
        {
            string root = ConfigurationManager.AppSettings["url_api"];
            return $"http://{root}/co/app/";
        }

    }
}