﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using InterceptorWS.Models;
using System.Configuration;

namespace InterceptorWS.Lib
{
    public class OfferDB
    {
        protected List<OfferId> OfferIdList = new List<OfferId>();

        public List<OfferId> GetConfidencialOffer(int CompanyId, int TotalOffer)
        {
            SqlConnection Connexion = new SqlConnection(SqlConnectionString());
            //Store procedure
            SqlCommand Command = new SqlCommand($"EXEC InterceptorOfferConfidencial {CompanyId}, {TotalOffer}", Connexion);
            
            Connexion.Open();  
            SqlDataReader Reader = Command.ExecuteReader();
             
            while (Reader.Read())
            {
              OfferIdList.Add(new OfferId { Id = Convert.ToInt32(Reader[0]) });
            }

            Reader.Close(); 
            Connexion.Close();

            return OfferIdList;
        }

        protected string SqlConnectionString()
        {
            return ConfigurationManager.AppSettings["elempleo_connection"];
        }
    }
}