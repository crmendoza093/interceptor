﻿using System.Web.Http;
using InterceptorWS.HTTP_Client;
using InterceptorWS.Helper;

namespace InterceptorWS.Controllers
{
    [RoutePrefix("api/jobofferdetail")]
    public class JobOfferDetailController : ApiController
    {
        [Route("getjobofferdata_v2")]
        public IHttpActionResult GetJobOfferData_V2(int jobOfferId, bool isLoggued, int personId)
        {
            WebApiClient Client = new WebApiClient();
            Rules BussinesRules = new Rules();

            //get response old api
            var Response = Client.GetOfferData(jobOfferId, isLoggued, personId);

            if (Response != null)
            {
                //data proccessed for hide data sensitive
                dynamic JsonProccessed = BussinesRules.SensitiveDataRule(Response);
                return Ok(JsonProccessed);
            }
            else
            {
                return BadRequest();
            }
        }

        [Route("getjobofferdata")]
        public IHttpActionResult GetJobOfferData(int jobOfferId, bool isLoggued, int personId)
        {
            WebApiClient Client = new WebApiClient();
            Rules BussinesRules = new Rules();

            //get response old api
            var Response = Client.GetOfferData(jobOfferId, isLoggued, personId);

            if (Response != null)
            {
                //data proccessed for hide data sensitive
                dynamic JsonProccessed = BussinesRules.SensitiveDataRule(Response);
                return Ok(JsonProccessed);
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
