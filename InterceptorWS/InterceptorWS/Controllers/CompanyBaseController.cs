﻿using InterceptorWS.Helper;
using InterceptorWS.HTTP_Client;
using InterceptorWS.Lib;
using InterceptorWS.Models;
using System.Collections.Generic;
using System.Web.Http;

namespace InterceptorWS.Controllers
{
    [RoutePrefix("api/CompanyBase")]
    public class CompanyBaseController : ApiController
    {
        public IHttpActionResult GetLastOffersApi(int CompanyId, int TotalOffers)
        {
            WebApiClient Client = new WebApiClient();
            Rules BussinesRules = new Rules();
            OfferDB cnx = new OfferDB();

            //get response old api
            var Response = Client.GetLastOffer(CompanyId, TotalOffers);
            //Get confidencial offer to database
            List<OfferId> OfferList = cnx.GetConfidencialOffer(CompanyId, TotalOffers);

            if (Response != null)
            {
                //data proccessed for hide data sensitive
                dynamic JsonProccessed = BussinesRules.ConfidencialOffer(Response, OfferList);
                return Ok(JsonProccessed);
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
